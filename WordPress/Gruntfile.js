module.exports = function (grunt) {
    'use strict';

    var globalConfig = {
        wpDestination: 'C:/xampp/htdocs/engineeredadhesive/wp-content/themes/eas-theme'
    };

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        globalConfig: globalConfig,
        pkg: grunt.file.readJSON('package.json'),

        autoprefixer: {
            files: {
                './dist/assets/css/style.css': './dist/assets/css/style.css'
            }
        },

        clean: {
            options: {
                force: true
            },
            prebuild: ['./dist/*', '<%= globalConfig.wpDestination %>/*'],
            beforemaincopy: ['./dist/bower_components'],
            afterbuild: ['./dist']
        },

        concat: {
            generated: {
                options: {
                    sourceMap: true
                }
            }
        },

        copy: {
            app: {
                files: [
                    {expand: true, cwd: './src/', src: ['partials/**', '!**/*.scss'], dest: './dist/'},
                    {expand: true, cwd: './src/', src: ['includes/**'], dest: './dist/'},
                    {expand: true, cwd: './src/', src: ['page_templates/**'], dest: './dist/'},
                    {expand: true, cwd: './src/', src: ['footer.php', 'functions.php', 'header.php', 'index.php', 'sidebar.php', 'style.css'], dest: './dist/'},
                    {expand: true, cwd: './src/assets/', src: '**', dest: './dist/assets'},
                    {expand: true, cwd: './src/bower_components/', src: '**', dest: './dist/bower_components'},
                    {expand: true, cwd: './src/bower_components/font-awesome/fonts/', src: '*.*', dest: './dist/assets/fonts'},
                    {expand: true, cwd: './src/bower_components/bootstrap-sass/assets/fonts/bootstrap', src: '*.*', dest: './dist/assets/fonts'}
                ]
            },
            mainbuild: {
                options: {
                    force: true,
                    mode: '777'
                },
                files: [
                    {expand: true, cwd: './dist/', src: '**', dest: '<%= globalConfig.wpDestination %>'}
                ]
            }
        },

        eol: {
            app: {
                options: {
                    eol: 'lf',
                    replace: true
                },
                files: [{
                    src: ['./dist/footer.php']
                }]
            }
        },

        html2js: {
            app: {
                options: {
                    base: './dist',
                    existingModule: true,
                    singleModule: true,
                    htmlmin: {
                        collapseBooleanAttributes: true,
                        collapseWhitespace: true,
                        removeAttributeQuotes: true,
                        removeComments: true,
                        removeEmptyAttributes: true,
                        removeRedundantAttributes: true,
                        removeScriptTypeAttributes: true,
                        removeStyleLinkTypeAttributes: true
                    }
                },
                src: [
                    'dist/partials/**/*.html'
                ],
                dest: './dist/partials/templates.js',
                module: 'app'
            }
        },

        jshint: {
            options: {
                reporter: require('jshint-stylish')
            },
            all: [
                './src/partials/**/*.js'
            ]
        },

        ngsrc: {
            target: {
                cwd: 'dist/',
                src: ['partials/**/*.js'],
                dest: ['./dist/footer.php']
            }
        },

        replace: {
            map: {
                src: ['./dist/assets/js/*.js.map'],
                overwrite: true,
                replacements: [{
                    from: '../../dist/',
                    to: ''
                }]
            }
        },

        sass: {
            app: {
                options: {
                    noCache: true,
                    sourcemap: 'none',
                    style: 'compressed'
                },
                files: {
                    './dist/assets/css/style.css': './src/partials/startup/base.scss'
                }
            }
        },

        scsslint: {
            allFiles: [
                './src/**/*.scss'
            ],
            options: {
                bundleExec: false
            }
        },

        tinyimg: {
            app: {
                files: [{
                    expand: true,
                    cwd: './dist/assets/images/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: './dist/assets/images/'
                }]
            }
        },

        uglify: {
            generated: {
                options: {
                    sourceMap: true,
                    sourceMapIn: function(source) {
                        return source.replace('.js', '.js.map');
                    }
                }
            }
        },

        usemin: {
            options: {
                blockReplacements: {
                    js: function(block) {
                        return '<script src="<?php echo get_bloginfo(\'template_directory\'); ?>/' + block.dest + '"></script>'
                    }
                }
            },
            html: ['./dist/footer.php']
        },

        useminPrepare: {
            html: './dist/footer.php'
        },

        wiredep: {
            code: {
                src: [
                    './dist/footer.php'
                ],
                ignorePath: '../src/'
            },
            scss: {
                src: [
                    './src/partials/startup/base.scss'
                ],
                ignorePath: '../src/',
                overrides: {
                    "bootstrap-grid-ms": {
                        "main": "scss/_bootstrap-grid-ms.scss"
                    }
                }
            }
        }
    });

    grunt.registerTask('build', [
        'scsslint',
        'jshint',
        'clean:prebuild',
        'wiredep:scss',
        'sass',
        'autoprefixer',
        'copy:app',
        'html2js',
        'tinyimg',
        'wiredep:code',
        'ngsrc',
        'eol',
        'useminPrepare',
        'concat:generated',
        'uglify:generated',
        'usemin',
        'replace:map',
        'clean:beforemaincopy',
        'copy:mainbuild',
        'clean:afterbuild'
    ]);
};
