(function() {
    'use strict';

    angular.module('app', [
        'angular.vertilize',
        'ngStorage',
        'ui.bootstrap',
        'ui.router',
        'about',
        'common',
        'contact',
        'home',
        'product-details',
        'products',
        'services'
    ]);
})();