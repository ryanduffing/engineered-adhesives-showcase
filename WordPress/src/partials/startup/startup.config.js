(function() {
    'use strict';

    angular.module('app').config(startupConfig);

    startupConfig.$inject = ['$httpProvider', '$urlRouterProvider', 'serviceBase'];

    function startupConfig($httpProvider, $urlRouterProvider, serviceBase) {
        $httpProvider.interceptors.push(['$q', function ($q) {
            return {
                'request': function(config) {
                    return config || $q.when(config);
                },
                'response': function(response) {
                    return response;
                },
                'responseError': function(response) {
                    return $q.reject(response);
                }
            };
        }]);

        $urlRouterProvider
            .when('/', '/home')
            .when('', '/home')
            .otherwise('/home');
    }
})();