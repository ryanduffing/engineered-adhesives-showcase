(function() {
    'use strict';

    angular.module('app').run(runApp);

    runApp.$inject = ['$rootScope', 'productsData', 'aboutData', 'contactData', 'homeData', 'servicesData', '$window'];

    function runApp($rootScope, productsData, aboutData, contactData, homeData, servicesData, $window) {

        /////////////////////////////////////////////////////////////////

        init();

        function createApp() {

            /**
             * Listen for route changes.
             */
            $rootScope.$on('$stateChangeStart', function (event, toState) {

            });

            /**
             * Listen for error on route change and stop view loading
             */
            $rootScope.$on('$stateChangeError', function() {

            });

            /**
             * Listen for success on route change and set up new section
             */
            $rootScope.$on('$stateChangeSuccess', function(event, toState) {
                $window.scrollTo(0, 0);

                // set page data properties for future use
                aboutData.getAboutPageData();
                contactData.getContactUsPageData();
                homeData.getHomePageData();
                productsData.getCategories();
                productsData.getProductPageData(152);
                productsData.getProductPageData(154);
                productsData.getProductTypes();
                servicesData.getServicesPageData();
            });

            FastClick.attach(document.body);
        }

        function init() {
            createApp();
        }
    }
})();