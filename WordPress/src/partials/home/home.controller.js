(function() {
    'use strict';

    angular.module('home.controller', [])
        .controller('HomeController', HomeController);

    HomeController.$inject = ['homePageData', 'brands', 'templateDirectory'];

    function HomeController(homePageData, brands, templateDirectory) {
        var vm = this;

        // scope methods

        // scope variables
        vm.backgroundImage = homePageData.backgroundImage.url;
        vm.brands = [];
        vm.buttonIcon = homePageData.buttonIcon;
        vm.buttonLink = homePageData.buttonLink;
        vm.buttonText = homePageData.buttonText;
        vm.subtitle = homePageData.subtitle;
        vm.tagline = homePageData.tagline;
        vm.templateDirectory = templateDirectory;

        /////////////////////////////////////////////////////////////////

        init();

        function init() {
            var homeBrands = _.filter(brands, function(brand) {
                return brand.acf.onHomePage.indexOf('yes') > -1;
            });

            // only want max of six brands
            vm.brands = homeBrands.slice(0, 6);
        }
    }
})();