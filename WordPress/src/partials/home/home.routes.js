(function() {
    'use strict';

    angular.module('home')
        .config(homeRoutes);

    homeRoutes.$inject = ['$stateProvider'];

    function homeRoutes($stateProvider) {
        /**
         * @name vm
         */
        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: 'partials/home/views/home.html',
                controller: 'HomeController',
                controllerAs: 'vm',
                resolve: {
                    brands: ['productsData', function(productsData) {
                        return productsData.getBrands().then(function(data) {
                            return data;
                        });
                    }],
                    homePageData: ['homeData', function(homeData) {
                        return homeData.getHomePageData().then(function(data) {
                            return data;
                        });
                    }]
                }
            });
    }

})();