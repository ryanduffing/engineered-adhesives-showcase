(function() {
    'use strict';

    angular.module('home.data', [])
        .factory('homeData', homeData);

    homeData.$inject = ['$http', '$sessionStorage', '$q'];

    function homeData($http, $sessionStorage, $q) {
        var data = {
            getHomePageData: getHomePageData
        };

        return data;

        /////////////////////////////////////////////////////////////////

        function getHomePageData() {
            if(!$sessionStorage.homePageData) {
                return $http.get('wp-json/acf/v2/page/5').then(function(response) {
                    $sessionStorage.homePageData = response.data.acf;

                    return response.data.acf;
                });
            }
            else {
                var deferred = $q.defer();

                deferred.resolve($sessionStorage.homePageData);

                return deferred.promise;
            }
        }
    }
})();