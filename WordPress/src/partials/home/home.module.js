(function() {
    'use strict';

    angular.module('home', [
        'home.controller',
        'home.data',
        'home.service'
    ]);
})();