(function() {
    'use strict';

    angular.module('products', [
        'products.controller',
        'products.data',
        'products.service'
    ]);
})();