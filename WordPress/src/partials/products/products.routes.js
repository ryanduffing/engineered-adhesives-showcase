(function() {
    'use strict';

    angular.module('products')
        .config(productsRoutes);

    productsRoutes.$inject = ['$stateProvider'];

    function productsRoutes($stateProvider) {
        /**
         * @name vm
         */
        $stateProvider
            .state('products', {
                url: '/products/:productType',
                templateUrl: 'partials/products/views/products.html',
                controller: 'ProductsController',
                controllerAs: 'vm',
                resolve: {
                    isAdhesives: ['$stateParams', function($stateParams) {
                        return $stateParams.productType === 'adhesives';
                    }],
                    productPageData: ['productsData', '$stateParams', function(productsData, $stateParams) {
                        var id = $stateParams.productType === 'adhesives' ? 152 : 154;

                        return productsData.getProductPageData(id).then(function(data) {
                            return data;
                        });
                    }]
                }
            });
    }

})();