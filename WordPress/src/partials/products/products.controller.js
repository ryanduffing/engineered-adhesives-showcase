/*jshint loopfunc: true */
(function() {
    'use strict';

    angular.module('products.controller', [])
        .controller('ProductsController', ProductsController);

    ProductsController.$inject = ['$stateParams', 'productsData', 'isAdhesives', 'productPageData', 'templateDirectory', '$localStorage', '$state'];

    function ProductsController($stateParams, productsData, isAdhesives, productPageData, templateDirectory, $localStorage, $state) {
        var vm = this;

        // scope methods
        vm.changeShowAtTime = changeShowAtTime;
        vm.goToPage = goToPage;
        vm.goToProduct = goToProduct;
        vm.searchProducts = searchProducts;
        vm.toggleFilter = toggleFilter;
        vm.toggleFilterSection = toggleFilterSection;

        // scope variables
        vm.allBrandsSelected = true;
        vm.allCategoriesSelected = true;
        vm.allTypesSelected = true;
        vm.brands = [];
        vm.categories = [];
        vm.currentPage = 0;
        vm.currentlyLoading = true;
        vm.currentlyShowing = '';
        vm.filterMessage = 'Show Filters';
        vm.header = productPageData.header;
        vm.numberOfPagesAvailable = [];
        vm.products = [];
        vm.productKind = isAdhesives ? 'Adhesives' : 'Equipment';
        vm.productTypes = [];
        vm.searchQuery = '';
        vm.showAtTime = 12;
        vm.showFilters = false;
        vm.subheader = productPageData.subheader;
        vm.templateDirectory = templateDirectory;
        vm.totalProducts = '';
        vm.visibleProducts = [];

        // controller variables
        var origProducts = [];
        var productHolder = [];

        /////////////////////////////////////////////////////////////////

        init();

        function applyFilters() {
            // reset some global page variables
            vm.currentPage = 0;
            vm.searchQuery = '';

            // need container to hold onto products we'll allow use to sift through
            var productContainer = [];

            // get selected categories, types, and brand ids only
            var selectedBrands = _.filter(vm.brands, function(brand) {
                return brand.isSelected;
            });

            var selectedCategories = _.filter(vm.categories, function(category) {
                return category.isSelected;
            });

            var selectedTypes = _.filter(vm.productTypes, function(type) {
                return type.isSelected;
            });

            if(selectedBrands.length > 0 || selectedCategories.length > 0 || selectedTypes.length > 0) {
                var selectedBrandIds = _.pluck(_.pluck(selectedBrands, 'data'), 'ID');
                var selectedCategoryIds = _.pluck(_.pluck(selectedCategories, 'data'), 'id');
                var selectedTypeIds = _.pluck(_.pluck(selectedTypes, 'data'), 'id');

                for(var i = 0; i < origProducts.length; i++) {
                    var intersectedCategories = _.intersection(selectedCategoryIds, origProducts[i].categories);
                    var intersectedTypes = _.intersection(selectedTypeIds, origProducts[i].producttypes);
                    var hasBrand = selectedBrandIds.indexOf(origProducts[i].acf.brand[0].ID) !== -1;

                    if(selectedBrandIds.length > 0 && selectedCategoryIds.length > 0 && selectedTypeIds.length > 0) {
                        // match on all
                        if(intersectedCategories.length > 0 && intersectedTypes.length > 0 && hasBrand) {
                            productContainer.push(origProducts[i]);
                        }
                    }
                    else if (selectedBrandIds.length > 0 && selectedCategoryIds.length > 0) {
                        // only match on brand and categories
                        if(intersectedCategories.length > 0 && hasBrand) {
                            productContainer.push(origProducts[i]);
                        }
                    }
                    else if (selectedBrandIds.length > 0 && selectedTypeIds.length > 0) {
                        // only match on brand and types
                        if(intersectedTypes.length > 0 && hasBrand) {
                            productContainer.push(origProducts[i]);
                        }
                    }
                    else if (selectedCategoryIds.length > 0 && selectedTypeIds.length > 0) {
                        // only match on categories and types
                        if(intersectedTypes.length > 0 && intersectedCategories.length > 0) {
                            productContainer.push(origProducts[i]);
                        }
                    }
                    else if (selectedTypeIds.length > 0) {
                        // only match on types
                        if(intersectedTypes.length > 0 ) {
                            productContainer.push(origProducts[i]);
                        }
                    }
                    else if (selectedCategoryIds.length > 0) {
                        // only match on categories
                        if(intersectedCategories.length > 0) {
                            productContainer.push(origProducts[i]);
                        }
                    }
                    else {
                        // only matching on brands
                        if (hasBrand) {
                            productContainer.push(origProducts[i]);
                        }
                    }
                }

                vm.products = angular.copy(productContainer);
                productHolder = angular.copy(productContainer);
            }
            else {
                vm.products = angular.copy(origProducts);
                productHolder = angular.copy(origProducts);
            }

            vm.totalProducts = vm.products.length;
            vm.visibleProducts = vm.showAtTime === 0 ? vm.products : vm.products.slice(0, vm.showAtTime);
            vm.numberOfPagesAvailable = vm.showAtTime === 0 ? [] : new Array(Math.ceil(vm.products.length / vm.showAtTime));

            if(vm.products.length === 0) {
                vm.currentlyShowing = '0';
            }
            else if (vm.products.length === 1) {
                vm.currentlyShowing = '1';
            }
            else {
                vm.currentlyShowing = '1-' + vm.visibleProducts.length;
            }
        }

        function changeShowAtTime(time) {
            vm.showAtTime = time;

            if(time === 0) {
                vm.visibleProducts = angular.copy(vm.products);
                vm.numberOfPagesAvailable = new Array(4);
            }
            else {
                vm.visibleProducts = vm.products.slice(0, time);
                vm.numberOfPagesAvailable = new Array(Math.ceil(vm.products.length / time));
            }

            vm.currentlyShowing = '1-' + vm.visibleProducts.length;
            vm.currentPage = 0;
        }

        function goToPage(page) {
            vm.currentPage = page;

            // get offset of where to start grabbing results
            var offset = page * vm.showAtTime;
            vm.visibleProducts = vm.products.slice(offset, vm.showAtTime + offset);

            if(vm.visibleProducts.length < vm.showAtTime) {
                vm.currentlyShowing = (offset + 1) + '-' + vm.products.length;
            }
            else {
                vm.currentlyShowing = (offset + 1) + '-' + (vm.showAtTime + offset);
            }
        }

        function goToProduct(product) {
            $localStorage.selectedProduct = product;

            $state.go('product-details', {productType: vm.productKind, id: product.id});
        }

        function init() {
            productsData.getProductsByProductType($stateParams.productType).then(function(products) {
                vm.currentlyLoading = false;

                vm.products = angular.copy(products);

                origProducts = angular.copy(products);
                productHolder = angular.copy(products);

                vm.numberOfPagesAvailable = new Array(Math.ceil(products.length / 12));

                var categories = [];
                var productTypes = [];

                // extract the category ids
                var flattenedCategories = _.flatten(_.pluck(products, 'categories'));
                var categoryIds = _.uniq(flattenedCategories);

                // extract the product types ids
                var flattenedProductTypes = _.flatten(_.pluck(products, 'producttypes'));
                var productTypeIds = _.uniq(flattenedProductTypes);

                // extract the brands
                var flattenedBrands = _.flatten(_.pluck(_.pluck(products, 'acf'), 'brand'));
                var brands = _.uniq(flattenedBrands, function(brand) {
                    return brand.ID;
                });

                _.each(productTypeIds, function(id) {
                    productsData.getProductTypeById(id).then(function(thisProductType) {
                        var matchingProductCount = _.countBy(products, function(product) {
                            return product.producttypes.indexOf(id) !== -1;
                        });

                        productTypes.push({
                            'isSelected': false,
                            'data': thisProductType,
                            'total': matchingProductCount.true
                        });

                        if(productTypes.length === productTypeIds.length) {
                            vm.productTypes = _.sortBy(productTypes, function(product) {
                                return product.data.name;
                            });
                        }
                    });
                });

                _.each(categoryIds, function(id) {
                    productsData.getCategoryById(id).then(function(thisCategory) {
                        var matchingProductCount = _.countBy(products, function(product) {
                            return product.categories.indexOf(id) !== -1;
                        });

                        categories.push({
                            'isSelected': false,
                            'data': thisCategory,
                            'total': matchingProductCount.true
                        });

                        if(categories.length === categoryIds.length) {
                            vm.categories = _.sortBy(categories, function(category) {
                                return category.data.name;
                            });
                        }
                    });
                });

                brands = _.sortBy(brands, 'post_title');

                _.each(brands, function(brand) {
                    var matchingBrands = _.where(flattenedBrands, {ID: brand.ID});

                    vm.brands.push({
                        'isSelected': false,
                        'data': brand,
                        'total': matchingBrands.length
                    });
                });

                vm.totalProducts = vm.products.length;
                vm.visibleProducts = vm.products.slice(0, 12);
                vm.currentlyShowing = '1-' + vm.visibleProducts.length;
            });
        }

        function searchProducts() {
            // search available products that are visible due to the filters
            if(!vm.searchQuery) {
                vm.products = angular.copy(productHolder);
            }
            else {
                vm.products = _.filter(productHolder, function(product) {
                    return product.acf.name.toLowerCase().indexOf(vm.searchQuery.toLowerCase()) !== -1;
                });
            }

            vm.totalProducts = vm.products.length;
            vm.visibleProducts = vm.products.slice(0, vm.showAtTime);
            vm.numberOfPagesAvailable = new Array(Math.ceil(vm.products.length / vm.showAtTime));
            vm.currentPage = 0;

            if(vm.products.length === 0) {
                vm.currentlyShowing = '0';
            }
            else if (vm.products.length === 1) {
                vm.currentlyShowing = '1';
            }
            else {
                vm.currentlyShowing = '1-' + vm.visibleProducts.length;
            }
        }

        function toggleFilter(filterType, filter) {
            switch(filterType) {
                case 'brand':
                    var brand = _.find(vm.brands, function(currBrand) {
                        return currBrand.data.ID === filter.ID;
                    });

                    brand.isSelected = !brand.isSelected;

                    break;
                case 'category':
                    var category = _.find(vm.categories, function(currCategory) {
                        return currCategory.data.id === filter.id;
                    });

                    category.isSelected = !category.isSelected;

                    break;
                case 'type':
                    var type = _.find(vm.productTypes, function(currType) {
                        return currType.data.id === filter.id;
                    });

                    type.isSelected = !type.isSelected;

                    break;
            }

            applyFilters();
        }

        function toggleFilterSection() {
            vm.showFilters = !vm.showFilters;

            if(vm.showFilters ) {
                vm.filterMessage = 'Hide Filters';
            }
            else {
                vm.filterMessage = 'Show Filters';
            }
        }
    }
})();
