(function() {
    'use strict';

    angular.module('products.data', [])
        .factory('productsData', productsData);

    productsData.$inject = ['$http', '$q', '$sessionStorage'];

    function productsData($http, $q, $sessionStorage) {
        var data = {
            getBrands: getBrands,
            getCategories: getCategories,
            getCategoryById: getCategoryById,
            getProductPageData: getProductPageData,
            getProductsByProductType: getProductsByProductType,
            getProductTypes: getProductTypes,
            getProductTypeById: getProductTypeById
        };

        return data;

        /////////////////////////////////////////////////////////////////

        function getBrands() {
            if(!$sessionStorage.brands) {
                return $http.get('wp-json/wp/v2/brands').then(function(response) {
                    $sessionStorage.brands = response.data;

                    return response.data;
                });
            }
            else {
                return manuallyResolve($sessionStorage.brands);
            }
        }

        function getCategories() {
            if(!$sessionStorage.categories) {
                return $http.get('wp-json/wp/v2/categories').then(function(response) {
                    $sessionStorage.categories = response.data;

                    return response.data;
                });
            }
            else {
                return manuallyResolve($sessionStorage.categories);
            }
        }

        function getCategoryById(id) {
            // search locally first
            if(!$sessionStorage.categories) {
                return retrieveCategory(id);
            }
            else {
                var category = _.find($sessionStorage.categories, function(cat) {
                    return cat.id === id;
                });

                if(!category) {
                    return retrieveCategory(id);
                }
                else {
                    return manuallyResolve(category);
                }
            }
        }

        function getProductsByProductType(type) {
            var deferred = $q.defer();

            getTheProducts(type, 1, [], deferred);

            return deferred.promise;
        }

        function getTheProducts(type, pageNumber, collection, deferred) {
            $http.get('wp-json/wp/v2/' + type + '?per_page=50&page=' + pageNumber).then(function(response) {
                if(response.data.length === 0) {
                    deferred.resolve(collection);
                }
                else {
                    collection = collection.concat(response.data);
                    getTheProducts(type, pageNumber += 1, collection, deferred);
                }
            });
        }

        function getProductPageData(id) {
            var storageType = id === 152 ? 'adhesives' : 'equipment';

            if(!$sessionStorage[storageType]) {
                return $http.get('wp-json/acf/v2/page/' + id).then(function(response) {
                    $sessionStorage[storageType] = response.data.acf;

                    return response.data.acf;
                });
            }
            else {
                return manuallyResolve($sessionStorage[storageType]);
            }
        }

        function getProductTypes() {
            if(!$sessionStorage.productTypes) {
                return $http.get('wp-json/wp/v2/producttypes').then(function(response) {
                    $sessionStorage.productTypes = response.data;

                    return response.data;
                });
            }
            else {
                return manuallyResolve($sessionStorage.productPageData);
            }
        }

        function getProductTypeById(id) {
            // search locally first
            if(!$sessionStorage.productTypes) {
                return retrieveProductType(id);
            }
            else {
                var productType = _.find($sessionStorage.productTypes, function(type) {
                    return type.id === id;
                });

                if(!productType) {
                    return retrieveProductType(id);
                }
                else {
                    return manuallyResolve(productType);
                }
            }
        }

        function manuallyResolve(item) {
            var deferred = $q.defer();

            deferred.resolve(item);

            return deferred.promise;
        }

        function retrieveCategory(id) {
            return $http.get('wp-json/wp/v2/categories/' + id).then(function(response) {
                return response.data;
            });
        }

        function retrieveProductType(id) {
            return $http.get('wp-json/wp/v2/producttypes/' + id).then(function(response) {
                return response.data;
            });
        }
    }
})();