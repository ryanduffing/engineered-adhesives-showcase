(function() {
    'use strict';

    angular.module('product-details.controller', [])
        .controller('ProductDetailsController', ProductDetailsController);

    ProductDetailsController.$inject = ['selectedProduct', 'productsData', 'templateDirectory'];

    function ProductDetailsController(selectedProduct, productsData, templateDirectory) {
        var vm = this;

        // scope methods

        // scope variables
        vm.categoriesHeader = '';
        vm.product = selectedProduct;
        vm.productCategories = [];
        vm.productTypes = [];
        vm.templateDirectory = templateDirectory;
        vm.typesHeader = '';

        /////////////////////////////////////////////////////////////////

        init();

        function getProductCategories() {
            _.each(selectedProduct.categories, function(id) {
                productsData.getCategoryById(id).then(function(category) {
                    vm.productCategories.push(category);
                });
            });
        }

        function getProductTypes() {
            _.each(selectedProduct.producttypes, function(id) {
                productsData.getProductTypeById(id).then(function(type) {
                    vm.productTypes.push(type);
                });
            });
        }

        function init() {
            getProductCategories();
            getProductTypes();

            vm.categoriesHeader = selectedProduct.categories.length > 1 ? 'categories:' : 'category:';
            vm.typesHeader = selectedProduct.producttypes.length > 1 ? 'types:' : 'type:';
        }
    }
})();