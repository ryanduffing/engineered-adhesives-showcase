(function() {
    'use strict';

    angular.module('product-details', [
        'product-details.controller',
        'product-details.data',
        'product-details.service'
    ]);
})();