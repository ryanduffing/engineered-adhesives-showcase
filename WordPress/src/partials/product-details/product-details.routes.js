(function() {
    'use strict';

    angular.module('product-details')
        .config(productDetailsRoutes);

    productDetailsRoutes.$inject = ['$stateProvider'];

    function productDetailsRoutes($stateProvider) {
        /**
         * @name vm
         */
        $stateProvider
            .state('product-details', {
                url: '/product-details/:productType/:id',
                templateUrl: 'partials/product-details/views/product-details.html',
                controller: 'ProductDetailsController',
                controllerAs: 'vm',
                resolve: {
                    selectedProduct: ['productDetailsData', '$stateParams', function(productDetailsData, $stateParams) {
                        return productDetailsData.getProduct($stateParams.productType, parseInt($stateParams.id)).then(function(product) {
                            return product;
                        });
                    }]
                }
            });
    }

})();