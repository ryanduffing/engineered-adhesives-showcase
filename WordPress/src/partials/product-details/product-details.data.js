(function() {
    'use strict';

    angular.module('product-details.data', [])
        .factory('productDetailsData', productDetailsData);

    productDetailsData.$inject = ['$http', '$q', '$sessionStorage'];

    function productDetailsData($http, $q, $sessionStorage) {
        var data = {
            getProduct: getProduct
        };

        return data;

        /////////////////////////////////////////////////////////////////

        function getProduct(productType, id) {
            // first see if we have the product in $localStorage
            var localProduct = $sessionStorage.selectedProduct;

            if(localProduct && localProduct.id === id) {
                var deferred = $q.defer();

                deferred.resolve(localProduct);

                return deferred.promise;
            }
            else {
                return $http.get('wp-json/wp/v2/' + productType + '/' + id).then(function(response) {
                    $sessionStorage.selectedProduct = response.data;

                    return response.data;
                });
            }
        }
    }
})();