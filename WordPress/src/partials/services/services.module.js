(function() {
    'use strict';

    angular.module('services', [
        'services.controller',
        'services.data',
        'services.service'
    ]);
})();