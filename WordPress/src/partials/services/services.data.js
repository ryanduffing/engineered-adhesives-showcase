(function() {
    'use strict';

    angular.module('services.data', [])
        .factory('servicesData', servicesData);

    servicesData.$inject = ['$http', '$sessionStorage', '$q'];

    function servicesData($http, $sessionStorage, $q) {
        var data = {
            getServicesPageData: getServicesPageData
        };

        return data;

        /////////////////////////////////////////////////////////////////

        function getServicesPageData() {
            if(!$sessionStorage.servicesPageData) {
                return $http.get('wp-json/acf/v2/page/40').then(function(response) {
                    $sessionStorage.servicesPageData = response.data.acf;

                    return response.data.acf;
                });
            }
            else {
                var deferred = $q.defer();

                deferred.resolve($sessionStorage.servicesPageData);

                return deferred.promise;
            }
        }
    }
})();