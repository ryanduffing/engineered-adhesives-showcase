(function() {
    'use strict';

    angular.module('services')
        .config(servicesRoutes);

    servicesRoutes.$inject = ['$stateProvider'];

    function servicesRoutes($stateProvider) {
        /**
         * @name vm
         */
        $stateProvider
            .state('services', {
                url: '/services',
                templateUrl: 'partials/services/views/services.html',
                controller: 'ServicesController',
                controllerAs: 'vm',
                resolve: {
                    servicesPageData: ['servicesData', function(servicesData) {
                        return servicesData.getServicesPageData().then(function(data) {
                            return data;
                        });
                    }]
                }
            });
    }

})();