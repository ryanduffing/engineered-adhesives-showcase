(function() {
    'use strict';

    angular.module('services.controller', [])
        .controller('ServicesController', ServicesController);

    ServicesController.$inject = ['servicesPageData'];

    function ServicesController(servicesPageData) {
        var vm = this;

        // scope methods

        // scope variables
        vm.services = servicesPageData.services;
        vm.servicesHeader = servicesPageData.header;
        vm.servicesSubHeader = servicesPageData.subheader;

        /////////////////////////////////////////////////////////////////

        init();

        function init() {

        }
    }
})();