(function () {
    'use strict';

    angular.module('focus-me', [])
        .directive('focusMe', focusMeDirective);

    focusMeDirective.$inject = [];

    function focusMeDirective() {
        return {
            scope: {
                trigger: '=focusMe'
            },
            link: function(scope, element) {
                scope.$watch('trigger', function(value) {
                    if(value === true) {
                        element[0].focus();
                        scope.trigger = false;
                    }
                });
            }
        };
    }
})();