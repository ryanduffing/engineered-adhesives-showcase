(function () {
    'use strict';

    angular.module('bc-swipe', [])
        .directive('bcSwipe', bcSwipeDirective);

    bcSwipeDirective.$inject = [];

    function bcSwipeDirective() {
        return {
            scope: {
                bcSwipe: '@'
            },
            link: function(scope) {
                $(scope.bcSwipe).bcSwipe({threshold: 50});
            }
        };
    }
})();