(function () {
    'use strict';

    angular.module('carousel-sliding', [])
        .directive('carouselSliding', carouselSlidingDirective);

    carouselSlidingDirective.$inject = ['$window'];

    function carouselSlidingDirective($window) {
        return {
            link: function(scope, element) {
                slidingResize();

                angular.element($window).bind('resize', slidingResize);

                function slidingResize() {
                    if($(window).width() < 992) {
                        $(element).addClass('slide');
                    }
                    else {
                        $(element).removeClass('slide');
                    }
                }
            }
        };
    }
})();