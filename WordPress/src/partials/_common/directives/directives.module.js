(function () {
    'use strict';

    angular.module('directives', [
        'carousel-sliding',
        'focus-me'
    ]);
})();