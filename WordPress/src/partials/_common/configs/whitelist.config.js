(function() {
    'use strict';

    angular.module('whitelist', [])
        .config(['$compileProvider', whitelist]);

    function whitelist($compileProvider) {
        // By default the only types of URLs whitelisted by Angular are http, https, ftp, and mailto. Any other
        // type of URL will show as unsafe by the browser.
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|geo|maps|chrome-extension|local|data):/);
    }
})();