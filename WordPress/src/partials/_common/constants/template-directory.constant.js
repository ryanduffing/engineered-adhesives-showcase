(function () {
    'use strict';

    angular.module('app')
        .constant('templateDirectory', getTemplateDirectory());

    function getTemplateDirectory() {
        return templateDirectory;
    }
})();