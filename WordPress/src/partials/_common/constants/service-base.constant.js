(function () {
    'use strict';

    angular.module('app')
        .constant('serviceBase', getServiceBase());

    function getServiceBase() {
        var servicePath = '/';
        var host = window.location.host;

        return 'http://' + host + servicePath;
    }
})();