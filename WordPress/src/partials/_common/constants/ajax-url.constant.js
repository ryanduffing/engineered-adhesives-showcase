(function () {
    'use strict';

    angular.module('app')
        .constant('ajaxUrl', getAjaxUrl());

    function getAjaxUrl() {
        return ajaxUrl;
    }
})();