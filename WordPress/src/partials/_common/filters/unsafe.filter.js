(function () {
    'use strict';

    angular.module('app')
        .filter('unsafe', ['$sce', function($sce) {
            return function(text) {
                return $sce.trustAsHtml(text);
            };
        }]);
})();