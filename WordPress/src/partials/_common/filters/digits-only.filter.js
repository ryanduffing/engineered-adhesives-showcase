(function () {
    'use strict';

    angular.module('app')
        .filter('digitsOnly', [function() {
            return function(text) {
                if (!angular.isString(text)) {
                    return text;
                }

                return text.replace(/\D+/g, "");
            };
        }]);
})();