(function () {
    'use strict';

    angular.module('common', [
        'configs',
        'directives'
    ]);
})();