(function() {
    'use strict';

    angular.module('contact.controller', [])
        .controller('ContactController', ContactController);

    ContactController.$inject = ['contactUsPageData', 'contactData', 'templateDirectory', '$timeout'];

    function ContactController(contactUsPageData, contactData, templateDirectory, $timeout) {
        var vm = this;

        // scope methods
        vm.submitForm = submitForm;

        // scope variables
        vm.businessAddressHeader = contactUsPageData.businessAddressHeader;
        vm.businessCity = contactUsPageData.businessCity;
        vm.businessMapLink = contactUsPageData.businessMapLink;
        vm.businessPostalCode = contactUsPageData.businessPostalCode;
        vm.businessState = contactUsPageData.businessState;
        vm.businessStreetAddress = contactUsPageData.businessStreetAddress;
        vm.emailAddressHeader = contactUsPageData.emailAddressHeader;
        vm.emailAddressOne = contactUsPageData.emailAddressOne;
        vm.emailAddressTwo = contactUsPageData.emailAddressTwo ? contactUsPageData.emailAddressTwo : '';
        vm.emailPlaceholder = contactUsPageData.emailPlaceholder;
        vm.formSubmitting = false;
        vm.header = contactUsPageData.header;
        vm.mailingAddressHeader = contactUsPageData.mailingAddressHeader ? contactUsPageData.mailingAddressHeader: '';
        vm.mailingCity = contactUsPageData.mailingCity ? contactUsPageData.mailingCity : '';
        vm.mailingMapLink = contactUsPageData.mailingMapLink;
        vm.mailingPostalCode = contactUsPageData.mailingPostalCode ? contactUsPageData.mailingPostalCode : '';
        vm.mailingState = contactUsPageData.mailingState ? contactUsPageData.mailingState : '';
        vm.mailingStreetAddress = contactUsPageData.mailingStreetAddress ? contactUsPageData.mailingStreetAddress : '';
        vm.messagePlaceholder = contactUsPageData.messagePlaceholder;
        vm.namePlaceholder = contactUsPageData.namePlaceholder;
        vm.phoneNumberHeader = contactUsPageData.phoneNumberHeader;
        vm.phoneNumberOne = contactUsPageData.phoneNumberOne;
        vm.phoneNumberTwo = contactUsPageData.phoneNumberTwo ? contactUsPageData.phoneNumberTwo : '';
        vm.spinningIcon = false;
        vm.subheader = contactUsPageData.subheader;
        vm.submitButtonText = contactUsPageData.submitButtonText;
        vm.templateDirectory = templateDirectory;

        // form fields
        vm.formName = '';
        vm.formMessage = '';
        vm.formEmail = '';

        /////////////////////////////////////////////////////////////////

        init();

        function init() {

        }

        function submitForm() {
            vm.submitButtonText = 'Sending...';
            vm.formSubmitting = true;
            vm.spinningIcon = true;

            contactData.sendMessage(vm.formName, vm.formEmail, vm.formMessage).then(function(response) {
                $timeout(function() {
                    vm.spinningIcon = false;

                    if(response.indexOf('sent success' > -1)) {
                        vm.formEmail = '';
                        vm.formName = '';
                        vm.formMessage = '';
                        vm.submitButtonText = 'Thank you!';
                    }
                    else {
                        vm.submitButtonText = 'Error! Please try again.';
                    }



                    $timeout(function() {
                        vm.submitButtonText = contactUsPageData.submitButtonText;
                        vm.formSubmitting = false;
                    }, 2000);
                }, 1000);
            }, function() {
                $timeout(function() {
                    vm.submitButtonText = 'Error! Please try again.';
                    vm.spinningIcon = false;

                    $timeout(function() {
                        vm.submitButtonText = contactUsPageData.submitButtonText;
                        vm.formSubmitting = false;
                    }, 2000);
                }, 1000);
            });
        }
    }
})();