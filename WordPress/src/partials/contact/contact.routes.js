(function() {
    'use strict';

    angular.module('contact')
        .config(contactRoutes);

    contactRoutes.$inject = ['$stateProvider'];

    function contactRoutes($stateProvider) {
        /**
         * @name vm
         */
        $stateProvider
            .state('contact', {
                url: '/contact',
                templateUrl: 'partials/contact/views/contact.html',
                controller: 'ContactController',
                controllerAs: 'vm',
                resolve: {
                    contactUsPageData: ['contactData', function(contactData) {
                        return contactData.getContactUsPageData().then(function(data) {
                            return data;
                        });
                    }]
                }
            });
    }

})();