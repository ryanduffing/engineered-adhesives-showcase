(function() {
    'use strict';

    angular.module('contact.data', [])
        .factory('contactData', contactData);

    contactData.$inject = ['$http', '$sessionStorage', '$q', 'ajaxUrl'];

    function contactData($http, $sessionStorage, $q, ajaxUrl) {
        var data = {
            getContactUsPageData: getContactUsPageData,
            sendMessage: sendMessage
        };

        return data;

        /////////////////////////////////////////////////////////////////

        function getContactUsPageData() {
            if(!$sessionStorage.contactPageData) {
                return $http.get('wp-json/acf/v2/page/53').then(function(response) {
                    $sessionStorage.contactPageData = response.data.acf;

                    return response.data.acf;
                });
            }
            else {
                var deferred = $q.defer();

                deferred.resolve($sessionStorage.contactPageData);

                return deferred.promise;
            }
        }

        function sendMessage(name, email, message) {
            return $http({
                method: 'POST',
                url: ajaxUrl,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                transformRequest: function(obj) {
                    var str = [];
                    for(var p in obj)
                        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                    return str.join("&");
                },
                data: {
                    name: name,
                    email: email,
                    message: message,
                    action: 'sendmail'
                }
            }).then(function(response) {
                return response.data;
            }, function(error) {
                return error;
            });
        }
    }
})();