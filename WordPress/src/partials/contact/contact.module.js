(function() {
    'use strict';

    angular.module('contact', [
        'contact.controller',
        'contact.data',
        'contact.service'
    ]);
})();