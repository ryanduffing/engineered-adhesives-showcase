(function() {
    'use strict';

    angular.module('about.controller', [])
        .controller('AboutController', AboutController);

    AboutController.$inject = ['aboutPageData'];

    function AboutController(aboutPageData) {
        var vm = this;

        // scope methods

        // scope variables
        vm.aboutUsSlides = aboutPageData.aboutUsSlides;
        vm.aboutUsHeader = aboutPageData.header;
        vm.aboutUsSubHeader = aboutPageData.subheader;

        /////////////////////////////////////////////////////////////////

        init();

        function init() {

        }
    }
})();