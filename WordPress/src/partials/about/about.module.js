(function() {
    'use strict';

    angular.module('about', [
        'about.controller',
        'about.data',
        'about.service'
    ]);
})();