(function() {
    'use strict';

    angular.module('about')
        .config(aboutRoutes);

    aboutRoutes.$inject = ['$stateProvider'];

    function aboutRoutes($stateProvider) {
        /**
         * @name vm
         */
        $stateProvider
            .state('about', {
                url: '/about',
                templateUrl: 'partials/about/views/about.html',
                controller: 'AboutController',
                controllerAs: 'vm',
                resolve: {
                    aboutPageData: ['aboutData', function(aboutData) {
                        return aboutData.getAboutPageData().then(function(data) {
                            return data;
                        });
                    }]
                }
            });
    }

})();