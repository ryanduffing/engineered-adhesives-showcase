(function() {
    'use strict';

    angular.module('about.data', [])
        .factory('aboutData', aboutData);

    aboutData.$inject = ['$http', '$sessionStorage', '$q'];

    function aboutData($http, $sessionStorage, $q) {
        var data = {
            getAboutPageData: getAboutPageData
        };

        return data;

        /////////////////////////////////////////////////////////////////

        function getAboutPageData() {
            if(!$sessionStorage.aboutPageData) {
                return $http.get('wp-json/acf/v2/page/28').then(function(response) {
                    $sessionStorage.aboutPageData = response.data.acf;

                    return response.data.acf;
                });
            }
            else {
                var deferred = $q.defer();

                deferred.resolve($sessionStorage.aboutPageData);

                return deferred.promise;
            }
        }
    }
})();