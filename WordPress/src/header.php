<!DOCTYPE html>
<html ng-app="app">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
        <meta name="keywords" content="<?php echo get_theme_mod('site_keywords'); ?>">
        <meta name="description" content="<?php echo get_theme_mod('site_description'); ?>">
        <meta itemprop="name" content="<?php echo get_theme_mod('site_title'); ?>">
        <meta itemprop="description" content="<?php echo get_theme_mod('site_description'); ?>">
        <title><?php echo get_theme_mod('site_title'); ?></title>
        <link rel="icon" type="image/png" href="<?php echo get_bloginfo('template_directory'); ?>/assets/images/favicon.ico">

        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href="<?php echo get_bloginfo('template_directory'); ?>/assets/css/style.css" rel="stylesheet" type="text/css">
        <script src="//code.jquery.com/jquery-2.2.4.min.js"></script>
    </head>
    <body>
        <div class="eyebrow-nav">
            <div class="container">
                <div class="contact-info">
                    <?php if(!empty(get_theme_mod('navbar_phone'))) { ?>
                        <a href="tel:<?php echo get_theme_mod('navbar_raw_phone'); ?>">
                            <i class="fa fa-phone"></i>
                            <span><?php echo get_theme_mod('navbar_phone'); ?></span>
                        </a>
                    <?php } ?>
                    <?php if(!empty(get_theme_mod('navbar_address'))) { ?>
                        <a target="_blank" href="<?php echo get_theme_mod('navbag_google_map_link'); ?>">
                            <i class="fa fa-map-marker"></i>
                            <span><?php echo get_theme_mod('navbar_address'); ?></span>
                        </a>
                    <?php } ?>
                </div>
                <div class="social-email">
                    <ul>
                        <?php if(!empty(get_theme_mod('facebook_url'))) { ?>
                            <li>
                                <a target="_blank" href="<?php echo get_theme_mod('facebook_url'); ?>" class="social-icon">
                                    <i class="fa fa-facebook-square"></i>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if(!empty(get_theme_mod('google_plus_url'))) { ?>
                            <li>
                                <a target="_blank" href="<?php echo get_theme_mod('google_plus_url'); ?>" class="social-icon">
                                    <i class="fa fa-google-plus-square"></i>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if(!empty(get_theme_mod('linkedin_url'))) { ?>
                            <li>
                                <a target="_blank" href="<?php echo get_theme_mod('linkedin_url'); ?>" class="social-icon">
                                    <i class="fa fa-linkedin-square"></i>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if(!empty(get_theme_mod('twitter_url'))) { ?>
                            <li>
                                <a target="_blank" href="<?php echo get_theme_mod('twitter_url'); ?>" class="social-icon">
                                    <i class="fa fa-twitter-square"></i>
                                </a>
                            </li>
                        <?php } ?>
                        <?php if(!empty(get_theme_mod('navbar_email'))) { ?>
                            <li>
                                <a href="mailto:<?php echo get_theme_mod('navbar_email'); ?>">
                                    <i class="fa fa-envelope"></i>
                                    <span><?php echo get_theme_mod('navbar_email'); ?></span>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#engineered-adhesive-navbar" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span>MENU</span>
                    </button>
                    <a class="navbar-brand" ui-sref="home">
                        <img alt="logo" src="<?php echo get_bloginfo('template_directory'); ?>/assets/images/main-logo.png" />
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="engineered-adhesive-navbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a ui-sref="home">Home</a></li>
                        <li><a ui-sref="about">About</a></li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">Products</a>
                            <ul class="dropdown-menu product-menu">
                                <li><a ui-sref="products({productType: 'adhesives'})">Adhesives</a></li>
                                <li><a ui-sref="products({productType: 'equipment'})">Equipment</a></li>
                            </ul>
                        </li>
                        <li><a ui-sref="services">Services</a></li>
                        <li><a ui-sref="contact">Contact</a></li>
                    </ul>
                    <div class="contact-mobile-nav">
                        <div class="contact-info">
                            <?php if(!empty(get_theme_mod('navbar_phone'))) { ?>
                                <a href="tel:<?php echo get_theme_mod('navbar_raw_phone'); ?>">
                                    <i class="fa fa-phone"></i>
                                    <span><?php echo get_theme_mod('navbar_phone'); ?></span>
                                </a>
                            <?php } ?>
                            <?php if(!empty(get_theme_mod('mobile_navbar_address'))) { ?>
                                <a target="_blank" href="<?php echo get_theme_mod('navbar_google_map_link'); ?>">
                                    <i class="fa fa-map-marker"></i>
                                    <span><?php echo get_theme_mod('mobile_navbar_address'); ?></span>
                                </a>
                            <?php } ?>
                            <?php if(!empty(get_theme_mod('navbar_email'))) { ?>
                                <a href="mailto:<?php echo get_theme_mod('navbar_email'); ?>">
                                    <i class="fa fa-envelope"></i>
                                    <span><?php echo get_theme_mod('navbar_email'); ?></span>
                                </a>
                            <?php } ?>
                        </div>
                        <div class="social-email">
                            <ul>
                                <?php if(!empty(get_theme_mod('facebook_url'))) { ?>
                                    <li>
                                        <a target="_blank" href="<?php echo get_theme_mod('facebook_url'); ?>" class="social-icon">
                                            <i class="fa fa-facebook-square"></i>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if(!empty(get_theme_mod('google_plus_url'))) { ?>
                                    <li>
                                        <a target="_blank" href="<?php echo get_theme_mod('google_plus_url'); ?>" class="social-icon">
                                            <i class="fa fa-google-plus-square"></i>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if(!empty(get_theme_mod('linkedin_url'))) { ?>
                                    <li>
                                        <a target="_blank" href="<?php echo get_theme_mod('linkedin_url'); ?>" class="social-icon">
                                            <i class="fa fa-linkedin-square"></i>
                                        </a>
                                    </li>
                                <?php } ?>
                                <?php if(!empty(get_theme_mod('twitter_url'))) { ?>
                                    <li>
                                        <a target="_blank" href="<?php echo get_theme_mod('twitter_url'); ?>" class="social-icon">
                                            <i class="fa fa-twitter-square"></i>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>