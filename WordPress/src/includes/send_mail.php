<?php
send_mail_init();

////////////////////////////////////////////////////////

function send_mail_init() {
    add_action('wp_ajax_sendmail', 'send_my_mail');
    add_action('wp_ajax_nopriv_sendmail', 'send_my_mail');
}

function send_my_mail() {
    try {
        $field_name = $_POST['name'];
        $field_email = $_POST['email'];
        $field_message = $_POST['message'];
        $error = false;
        $error_message = 'Unknown error occurred.';

        if(!$error) {
            if(empty($field_name))
            {
                $error = true;
                $error_message = 'Name field is empty.';
            }
            else {
                $field_name = test_input($field_name);
            }
        }

        if(!$error) {
            if(empty($field_email))
            {
                $error = true;
                $error_message = 'Email field is empty.';
            }
            else
            {
                if (!filter_var($field_email, FILTER_VALIDATE_EMAIL)) {
                    $error = true;
                    $error_message = 'Email field is invalid.';
                }
                else {
                    $field_email = test_input($field_email);
                }
            }
        }

        if(!$error) {
            if(empty($field_message))
            {
                $error = true;
                $error_message = 'Message field is empty.';
            }
            else
            {
                $field_message = test_input($field_message);
            }
        }

        if(!$error)
        {
            $mail_to = 'info@engineeredadhesive.com';
            $mail_from = 'contact-form@engineeredadhesive.com';
            $subject = 'Message from a site visitor :'.$field_name;

            $body_message = 'From: '.$field_name."\n";
            $body_message .= 'E-mail: '.$field_email."\n";
            $body_message .= 'Message: '.$field_message."\n";

            $headers = 'From: '.$mail_from."\r\n";
            $headers .= 'Reply-To: '.$field_email."\r\n";

            $mail_status = mail($mail_to, $subject, $body_message, $headers);

            if($mail_status)
            {
                echo 'sent success';
            }
            else
            {
                echo 'error occurred';
            }
        }
        else {
            echo $error_message;
        }
    }
    catch (Exception $e) {
        echo $error_message;
    }
}

// Function for filtering input values.
function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

?>