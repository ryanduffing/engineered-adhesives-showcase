<?php

site_customizer_init();

////////////////////////////////////////////////////////

function footer_customizer($wp_customize) {
    $wp_customize->add_section('footer_section', array(
        'title' => __('Footer', 'limus'),
        'description' => 'Modify the Footer Information'
    ));

    $wp_customize->add_setting('footer_business_title', array(
        'default' => 'Engineered Adhesive Systems'
    ));

    $wp_customize->add_control('footer_business_title', array(
        'label' => __('Business Title', 'limus'),
        'section' => 'footer_section',
        'setting' => 'footer_business_title'
    ));
}

function navbar_customizer($wp_customize) {
    $wp_customize->add_section('navbar_section', array(
        'title' => __('Navbar', 'limus'),
        'description' => 'Modify the Navbar Information'
    ));

    $wp_customize->add_setting('navbar_email', array(
        'default' => ''
    ));

    $wp_customize->add_control('navbar_email', array(
        'label' => __('Email', 'limus'),
        'section' => 'navbar_section',
        'setting' => 'navbar_email'
    ));

    $wp_customize->add_setting('navbar_phone', array(
        'default' => ''
    ));

    $wp_customize->add_control('navbar_phone', array(
        'label' => __('Phone', 'limus'),
        'section' => 'navbar_section',
        'setting' => 'navbar_phone'
    ));

    $wp_customize->add_setting('navbar_raw_phone', array(
        'default' => ''
    ));

    $wp_customize->add_control('navbar_raw_phone', array(
        'label' => __('Phone (only digits)', 'limus'),
        'section' => 'navbar_section',
        'setting' => 'navbar_raw_phone'
    ));

    $wp_customize->add_setting('navbar_address', array(
        'default' => ''
    ));

    $wp_customize->add_control('navbar_address', array(
        'label' => __('Address', 'limus'),
        'section' => 'navbar_section',
        'setting' => 'navbar_address'
    ));

    $wp_customize->add_setting('mobile_navbar_address', array(
        'default' => ''
    ));

    $wp_customize->add_control('mobile_navbar_address', array(
        'label' => __('Address (separate on two lines)', 'limus'),
        'section' => 'navbar_section',
        'setting' => 'mobile_navbar_address',
        'type' => 'textarea'
    ));

    $wp_customize->add_setting('navbag_google_map_link', array(
        'default' => ''
    ));

    $wp_customize->add_control('navbag_google_map_link', array(
        'label' => __('Google Map Link', 'limus'),
        'section' => 'navbar_section',
        'setting' => 'navbag_google_map_link'
    ));
}

function site_customizer($wp_customize) {
    $wp_customize->add_section('site_section', array(
        'title' => __('Site', 'limus'),
        'description' => 'Modify the Site Information'
    ));

    $wp_customize->add_setting('site_title', array(
        'default' => 'Engineered Adhesive Systems'
    ));

    $wp_customize->add_control('site_title', array(
        'label' => __('Title', 'limus'),
        'section' => 'site_section',
        'setting' => 'site_title'
    ));

    $wp_customize->add_setting('site_description', array(
        'default' => ''
    ));

    $wp_customize->add_control('site_description', array(
        'label' => __('Description', 'limus'),
        'section' => 'site_section',
        'setting' => 'site_description',
        'type' => 'textarea'
    ));

    $wp_customize->add_setting('site_keywords', array(
        'default' => ''
    ));

    $wp_customize->add_control('site_keywords', array(
        'label' => __('Keywords', 'limus'),
        'section' => 'site_section',
        'setting' => 'site_keywords',
        'type' => 'textarea'
    ));
}

function site_customizer_init() {
    add_action('customize_register', 'site_customizer');
    add_action('customize_register', 'footer_customizer');
    add_action('customize_register', 'social_media_customizer');
    add_action('customize_register', 'navbar_customizer');
}

function social_media_customizer($wp_customize) {
    $wp_customize->add_section('social_media_section', array(
        'title' => __('Social Media', 'limus'),
        'description' => 'Modify the Social Media Information'
    ));

    $wp_customize->add_setting('facebook_url', array(
        'default' => ''
    ));

    $wp_customize->add_control('facebook_url', array(
        'label' => __('Facebook Url', 'limus'),
        'section' => 'social_media_section',
        'setting' => 'facebook_url'
    ));

    $wp_customize->add_setting('google_plus_url', array(
        'default' => ''
    ));

    $wp_customize->add_control('google_plus_url', array(
        'label' => __('Google Plus Url', 'limus'),
        'section' => 'social_media_section',
        'setting' => 'google_plus_url'
    ));

    $wp_customize->add_setting('linkedin_url', array(
        'default' => ''
    ));

    $wp_customize->add_control('linkedin_url', array(
        'label' => __('LinkedIn Url', 'limus'),
        'section' => 'social_media_section',
        'setting' => 'linkedin_url'
    ));

    $wp_customize->add_setting('twitter_url', array(
        'default' => ''
    ));

    $wp_customize->add_control('twitter_url', array(
        'label' => __('Twitter Url', 'limus'),
        'section' => 'social_media_section',
        'setting' => 'twitter_url'
    ));
}
?>