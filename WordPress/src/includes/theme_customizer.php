<?php

theme_customizer_init();

////////////////////////////////////////////////////////

function eas_customize_register($wp_customize) {
    $wp_customize->remove_section('static_front_page');
    $wp_customize->remove_section('title_tagline');
    $wp_customize->remove_section('themes');
}

function eas_customize_nav_register($wp_customize) {
    //check if WP_Customize_Nav_Menus object exist
    if (isset($wp_customize->nav_menus) && is_object($wp_customize->nav_menus)) {
        //Remove all the filters/actions registered in WP_Customize_Nav_Menus __construct
        remove_filter( 'customize_refresh_nonces', array( $wp_customize->nav_menus, 'filter_nonces' ) );
        remove_action( 'wp_ajax_load-available-menu-items-customizer', array( $wp_customize->nav_menus, 'ajax_load_available_items' ) );
        remove_action( 'wp_ajax_search-available-menu-items-customizer', array( $wp_customize->nav_menus, 'ajax_search_available_items' ) );
        remove_action( 'customize_controls_enqueue_scripts', array( $wp_customize->nav_menus, 'enqueue_scripts' ) );
        remove_action( 'customize_register', array( $wp_customize->nav_menus, 'customize_register' ), 11 );
        remove_filter( 'customize_dynamic_setting_args', array( $wp_customize->nav_menus, 'filter_dynamic_setting_args' ), 10, 2 );
        remove_filter( 'customize_dynamic_setting_class', array( $wp_customize->nav_menus, 'filter_dynamic_setting_class' ), 10, 3 );
        remove_action( 'customize_controls_print_footer_scripts', array( $wp_customize->nav_menus, 'print_templates' ) );
        remove_action( 'customize_controls_print_footer_scripts', array( $wp_customize->nav_menus, 'available_items_template' ) );
        remove_action( 'customize_preview_init', array( $wp_customize->nav_menus, 'customize_preview_init' ) );
        remove_filter( 'customize_dynamic_partial_args', array( $wp_customize->nav_menus, 'customize_dynamic_partial_args' ), 10, 2 );
    }
}

function theme_customizer_init() {
    // actions
    add_action('customize_register', 'eas_customize_register', 20);
    add_action('customize_register', 'eas_customize_nav_register', -1);
}
?>