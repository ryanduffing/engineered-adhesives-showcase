<?php

menu_item_init();

////////////////////////////////////////////////////////

function change_labels() {
    global $menu;
    $menu[10][0] = 'Pictures';
    echo '';
}

function change_media_library_title($translated) {
    $translated = str_ireplace('Media Library', 'Pictures', $translated);  // ireplace is PHP5 only
    return $translated;
}

function hide_new_page_buttons()
{
    global $current_screen;

    // only hide new page button from any user not a super admin
    if(($current_screen->id == 'page' && !isAdmin()) ||
        (isset($_GET['post_type']) && $_GET['post_type'] == 'page' && !isAdmin()))
    {
        echo '<style>.wrap .page-title-action{display:none;}</style>';
    }
}

function menu_item_init() {
    // actions
    add_action('admin_head', 'hide_new_page_buttons');
    add_action('admin_menu', 'change_labels');
    add_action('admin_menu', 'my_modify_menus', 999);
    add_action('wp_before_admin_bar_render', 'remove_admin_bar_links');

    // filters
    add_filter('gettext', 'change_media_library_title');
    add_filter('ngettext', 'change_media_library_title');
}

function my_modify_menus() {
    remove_menu_page('edit.php');
    remove_menu_page('edit-comments.php');
    remove_menu_page('index.php');
    remove_menu_page('tools.php');
    remove_submenu_page('upload.php', 'media-new.php');
    remove_submenu_page('themes.php', 'themes.php');
    remove_submenu_page('themes.php', 'theme-editor.php');
    remove_submenu_page('users.php', 'profile.php');
    remove_submenu_page('plugins.php', 'plugin-editor.php');
    remove_submenu_page('plugins.php', 'plugin-install.php');
    remove_submenu_page('users.php', 'user-new.php');

    // items to show up for admin only
    if(!isAdmin()) {
        remove_menu_page('edit.php?post_type=acf-field-group');
        remove_menu_page('admin.php');
        remove_menu_page('plugins.php');
        remove_menu_page('users.php');
        remove_menu_page('options-general.php');

        global $submenu;
        unset($submenu['edit.php?post_type=page'][10]);
    }
}

function remove_admin_bar_links() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('wp-logo');          // Remove the WordPress logo
    $wp_admin_bar->remove_menu('comments');         // Remove the comments link
    $wp_admin_bar->remove_menu('new-content');      // Remove the content link
    $wp_admin_bar->remove_menu('updates');          // Remove the updates link
    $wp_admin_bar->remove_menu('edit-profile');
    $wp_admin_bar->remove_menu('view-site');
    $wp_admin_bar->remove_menu('user-info');
    $wp_admin_bar->remove_node('my-account');

    $args = array(
        'id'     => 'logout',           // id of the existing child node (New > Post)
        'title'  => 'Logout',   // alter the title of existing node
        'parent' => 'top-secondary',    // set parent
    );
    $wp_admin_bar->add_node($args);
}
?>