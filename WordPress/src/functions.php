<?php
init();

////////////////////////////////////////////////////////

function cloneAdminRole() {
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
        $wp_roles = new WP_Roles();

    $adm = $wp_roles->get_role('administrator');
    $wp_roles->add_role('lower_admin', 'Lower Admin Role', $adm->capabilities);

    $role = get_role('lower_admin');
    $role->remove_cap('delete_pages');
    $role->remove_cap('delete_others_pages');
    $role->remove_cap('delete_published_pages');
}

function get_current_roles() {
    $roles = wp_get_current_user()->roles;

    return $roles;
}

function init() {
    cloneAdminRole();

    // each customizable section of the theme is in its own module folder
    require_once('includes/site_customizer.php');

    require_once('includes/dashboard_customizer.php');
    require_once('includes/menu_items.php');
    require_once('includes/send_mail.php');
    require_once('includes/theme_customizer.php');
}

function isAdmin() {
    $curr_roles = get_current_roles();

    foreach($curr_roles as $role) {
        if($role == 'administrator') {
            return true;
        }
    }

    return false;
}
?>