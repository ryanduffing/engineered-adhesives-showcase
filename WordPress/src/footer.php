        <footer>
            <div class="container">
                <div class="footer-logo">
                    <img alt="logo" src="<?php echo get_bloginfo('template_directory'); ?>/assets/images/logo_img.png" />
                </div>
                <?php if(!empty(get_theme_mod('footer_business_title'))) { ?>
                    <h2 class="footer-business"><?php echo get_theme_mod('footer_business_title'); ?></h2>
                <?php } ?>
                <p class="footer-message">
                    <a href="<?php echo get_bloginfo('template_directory'); ?>/assets/docs/eas_terms_of_sale.pdf">Terms of Sale</a>
                </p>
                <ul class="footer-social-media">
                    <?php if(!empty(get_theme_mod('facebook_url'))) { ?>
                        <li>
                            <a target="_blank" href="<?php echo get_theme_mod('facebook_url'); ?>"><i class="fa fa-facebook"></i></a>
                        </li>
                    <?php } ?>
                    <?php if(!empty(get_theme_mod('google_plus_url'))) { ?>
                        <li>
                            <a target="_blank" href="<?php echo get_theme_mod('google_plus_url'); ?>"><i class="fa fa-google-plus"></i></a>
                        </li>
                    <?php } ?>
                    <?php if(!empty(get_theme_mod('linkedin_url'))) { ?>
                        <li>
                            <a target="_blank" href="<?php echo get_theme_mod('linkedin_url'); ?>"><i class="fa fa-linkedin"></i></a>
                        </li>
                    <?php } ?>
                    <?php if(!empty(get_theme_mod('twitter_url'))) { ?>
                        <li>
                            <a target="_blank" href="<?php echo get_theme_mod('twitter_url'); ?>"><i class="fa fa-twitter"></i></a>
                        </li>
                    <?php } ?>
                </ul>
                <h4 class="footer-copyright">
                    <i class="fa fa-copyright"></i>
                    <span><?php echo date("Y"); ?></span>
                    <?php if(!empty(get_theme_mod('footer_business_title'))) { ?>
                        <span> | <?php echo get_theme_mod('footer_business_title'); ?></span>
                    <?php } ?>
                </h4>
            </div>
        </footer>
        <script>
            // expose template directory and ajax url globally so angularjs can pick it up
            var ajaxUrl = '<?php echo admin_url('admin-ajax.php'); ?>';
            var templateDirectory = '<?php echo get_bloginfo('template_directory'); ?>';

            function carouselSliding() {
                if($(window).width() < 992) {
                    $('#product-carousel').addClass('slide');
                }
                else {
                    $('#product-carousel').removeClass('slide');
                }
            }

            $(document).ready(function() {
                $(window).resize(carouselSliding);
            });

            $('.navbar-collapse ul li a').click(function(){
                if(!$(this).hasClass('dropdown-toggle')) {
                    $('.navbar-toggle:visible').click();
                }
            });
        </script>

        <!-- build:js assets/js/components.js -->
            <!-- bower:js -->
            <!-- endbower -->
        <!-- endbuild -->

        <!-- build:js assets/js/app.js -->
            <!-- ngsrc -->
        <!-- endbuild -->
    </body>
</html>